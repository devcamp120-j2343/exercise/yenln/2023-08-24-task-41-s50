var gBASE_URL = "https://food-ordering-fvo9.onrender.com/api";
var gOrderInformationDetail = [];
var gOrderList = [];
$(document).ready(function () {
    const gDATA = ['id', 'orderCode', 'fullName', 'methodPayment', 'foods', 'totalCost', 'createdAt', 'action'];
    const gSTT_COL = 0;
    const gORDER_CODE_COL = 1;
    const gFULL_NAME_COL = 2;
    const gMETHOD_PAYMENT_COL = 3;
    const gFOODS_COL = 4;
    const gTOTAL_COST_COL = 5;
    const gCREATE_DATE_COL = 6;
    const gACTION_COL = 7;
    var vTable = $("#order-table").DataTable({
        columns: [
            { data: gDATA[gSTT_COL] },
            { data: gDATA[gORDER_CODE_COL] },
            { data: gDATA[gFULL_NAME_COL] },
            { data: gDATA[gMETHOD_PAYMENT_COL] },
            { data: gDATA[gFOODS_COL] },
            { data: gDATA[gTOTAL_COST_COL] },
            { data: gDATA[gCREATE_DATE_COL] },
            { data: gDATA[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gFULL_NAME_COL,
                render: function (data, type, row) {
                    if (type == 'display') {
                        return `<p>${row.firstName} ${row.lastName}</p>`
                    }
                }
            },
            {
                targets: gTOTAL_COST_COL,
                render: function (data, type, row) {
                    if (type == 'display') {
                        var vFoods = row.foods;
                        var vTotal = 0;
                        for (let i in vFoods) {
                            vTotal += vFoods[i].price;
                        }
                        return `<p>$${vTotal}</p>`
                    }
                }
            },
            {
                targets: gFOODS_COL,
                render: function (data, type, row) {
                    if (type == 'display') {
                        return `<p>${row.foods.length}</p>`
                    }
                }
            },
            {
                targets: gACTION_COL,
                defaultContent: `
                    <a><i class="fas fa-edit edit-btn text-success" style="cursor:pointer"></i></a>
                    <a><i class="fa-solid fa-trash-can delete-btn text-danger" style="cursor:pointer"></i></a>`
            },

        ]
    })
    onPageLoading();

    function getIdByBtn(element) {
        var vRow = $(element).parents('tr');
        var vData = vTable.row(vRow).data();
        return vData.id;
    }

    function getOrderDetailById(paramId) {
        var vAPI_URL = gBASE_URL + "/orders/";

        $.ajax({
            url: vAPI_URL + paramId,
            type: 'GET',
            async: false,
            success: function (res) {
                console.log(res);
                thuThapDuLieu(res);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    }

    function loadInformationIntoTable(paramList) {
        vTable.clear();
        vTable.rows.add(paramList);
        vTable.draw();
    }

    function callApiToGetOrderList() {
        var vAPI_URL = gBASE_URL + "/orders";

        $.ajax({
            url: vAPI_URL,
            type: 'GET',
            async: false,
            success: function (res) {
                console.log(res);
                gOrderList = res;
            },
            error: function (error) {
                console.log(error);
            }
        })
    }

    function onPageLoading() {
        callApiToGetOrderList();
        setTimeout(loadInformationIntoTable(gOrderList), 300);
        $("#order-table").on("click", ".edit-btn", function () {
            onEditBtnClick(this);
        });
        $("#order-table").on("click", ".delete-btn", function () {
            onDeleteBtnClick(this);
        });
    }

    function onEditBtnClick(element) {
        var vId = getIdByBtn(element);
        $("#edit-admin-modal").modal("show");
        getOrderDetailById(vId);
        $("#edit-admin-modal").on("click", "#update-order-modal-btn", function () {
            var vInformationOrderDetail = {
                firstName: "",
                lastName: "",
                email: "",
                address: "",
                phone: "",
                methodPayment: "",
                voucherId: "",
            };
            readData(vInformationOrderDetail);
            var vDuLieuHopLe = validateInfor(vInformationOrderDetail);
            if (vDuLieuHopLe) {
                callApiToChangeInformation(vInformationOrderDetail, vId);
            }
        })
    }

    function callApiToChangeInformation(paramOrderDetail, paramId) {
        var vAPI_URL = gBASE_URL + "/orders/";
        $.ajax({
            url: vAPI_URL + paramId,
            type: 'PUT',
            contentType: "application/json",
            data: JSON.stringify(paramOrderDetail),
            success: function (res) {
                console.log(res);
                console.log('Successfully updated');
                window.location.reload();
            },
            error: function (error) {
                console.log(error.responseText)
            }
        })
    }

    function validateInfor(paramInfor) {
        if (paramInfor.firstName == "") {
            alert("Vui lòng điền [first name]");
            return false;
        }
        if (paramInfor.lastName == "") {
            alert("Vui lòng điền [last name]");
            return false;
        }
        if (paramInfor.email == "") {
            alert("Vui lòng điền [email]");
            return false;
        }
        if (!validateEmail(paramInfor.email)) {
            alert("Vui lòng điền đúng định dạng [email]");
            return false;
        }
        if (paramInfor.address == "") {
            alert("Vui lòng điền [địa chỉ]");
            return false;
        }
        if (paramInfor.phone == "") {
            alert("Vui lòng điền [sdt]");
            return false;
        }
        if (!validatePhoneNumber(paramInfor.phone)) {
            alert("Vui lòng điền đúng định dạng [sdt]");
            return false;
        }
        if (paramInfor.methodPayment == "0") {
            alert("Vui lòng chọn [methodPayment]");
            return false;
        }
        return true;
    }

    function validateEmail(email) {
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (email.match(validRegex)) {
            return true;
        }
        else {
            return false;
        }
    };
    function validatePhoneNumber(phoneNumber) {
        var validRegex = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
        if (phoneNumber.match(validRegex)) {
            return true;
        }
        else {
            return false;
        }
    }
    function readData(paramInfor) {
        paramInfor.lastName = $("#ho-va-dem-inp-edit").val();
        paramInfor.firstName = $("#ten-inp-edit").val();
        paramInfor.email = $("#email-inp-edit").val();
        paramInfor.phone = $("#phone-inp-edit").val();
        paramInfor.address = $("#address-inp-edit").val();
        paramInfor.methodPayment = $("#payment-method-select-edit").val();
        paramInfor.voucherId = $("#discount-inp-edit").val();
    }

    function thuThapDuLieu(paramOrder) {
        $("#ho-va-dem-inp-edit").val(paramOrder.lastName);
        $("#ten-inp-edit").val(paramOrder.firstName);
        $("#email-inp-edit").val(paramOrder.email);
        $("#phone-inp-edit").val(paramOrder.phone);
        $("#address-inp-edit").val(paramOrder.address);
        $("#payment-method-select-edit").val(paramOrder.methodPayment);
        $("#discount-inp-edit").val(paramOrder.voucherId);
        console.log(paramOrder);
    }

    function onDeleteBtnClick(element) {
        var vId = getIdByBtn(element);
        console.log(vId);
        $("#delete-admin-modal").modal('show');
        var text = "Order này sẽ bị xóa vĩnh viễn và không thể khôi phục";
        if (confirm(text) == true) {
            callApiToDeleteAnOrder(vId);
        }
        else {
            $("#delete-admin-modal").modal('hide');
        }
    }

    function callApiToDeleteAnOrder(paramId) {
        var vAPI_URL = gBASE_URL + "/orders/";
        $.ajax({
            url: vAPI_URL + paramId,
            type: 'DELETE',
            success: function (res) {
                alert("successfully deleted");
            },
            error: function (error) {
                console.log(error);
            }


    })
}});

