var gBASE_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/";
var gBASE_URL_2 = "https://food-ordering-fvo9.onrender.com/api";
var gShoppingBag = localStorage.getItem('shoppingBag') ? JSON.parse(localStorage.getItem('shoppingBag')) : [];
localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
var gVoucherList = [];
var gVoucherDiscountPercent = 0;

var gPizzaOrderList = [];

$(document).ready(function () {
    callAPIAndloadPizzaProductsIntoFE();
    callAPIAndloadBlogIntoFE();
    $('.list-pizza-kitchen-section').on('click', '.add-a-product', function () {
        addAProductIntoShoppingBag(this);
    })
    changeBagToYellow();
    loadOrderListIntoOrderPlace();

    $(".anProduct__quantityControl--btn-down").on('click', function () {
        reduceTheQuantity(this);
    })
    $(".anProduct__quantityControl--btn-up").on('click', function () {
        addTheQuantity(this);
    })
    $(".an-Product__removeItem").on('click', function () {
        removeItem(this);
    })
    onPageLoading();
    $("#total-voucher-btn").on("click", function () {
        checkVoucher();
    })

})

function onPageLoading() {
    loadTheTotalCostOfTheBill();
    calculateTheLastTotalCost();
    $("#check-out-order-btn").on('click', function () {
        $('#payment-modal').modal('show');
    })

}

function checkVoucherRightWrong(paramVoucherList, paramIdInp) {
    var vSubTotal = JSON.parse($("#sub-total-first").text());
    console.log(paramIdInp);
    var vResult = null;
    var vFound = false;
    var bI = 0;
    $("#voucher-result").html('');
    while (!vFound && bI < paramVoucherList.length) {
        if (paramVoucherList[bI].voucherCode == paramIdInp) {
            vFound = true;
            alert('voucher hợp lệ');
            vResult = paramVoucherList[bI];
            $("#voucher-result").html('Yes')
                .attr("data-is-selected", "Y");
            gVoucherDiscountPercent = paramVoucherList[bI].discount;

            var vDivision = gVoucherDiscountPercent / 100;

            var vTotal = vSubTotal + 20 - vDivision;
            $("#sub-total-with-coupon").html(vTotal);
        }
    }
    if (!vFound) {
        alert("Voucher không hợp lệ");
        $("#voucher-result").html('No')
            .attr("data-is-selected", "N");
    }
}

function checkVoucher() {
    var vIdVoucherInp = $("#total-voucher-input").val();
    var vSearchParams = new URLSearchParams(vIdVoucherInp);
    var vAPI_URL = gBASE_URL_2 + "/vouchers" + "?" + vSearchParams.toString();

    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        async: false,
        success: function (res) {
            console.log(res);
            gVoucherList = res;
            checkVoucherRightWrong(res, vIdVoucherInp);
        },
        error: function (error) {
            console.log(error);
        }
    })

}

function loadTheTotalCostOfTheBill() {
    $("#sub-total-first").html('');
    var vTotalDemo = 0;
    for (let i in gShoppingBag) {
        vTotalDemo += gShoppingBag[i].totalOfOneType;
    }
    $("#sub-total-first").html(vTotalDemo);
}

function addTheQuantity(paramElement) {
    var vQuantityPlace = getQuantityByParents(paramElement);
    var vIdOfPizza = $(paramElement).data('id');
    var vPizzaDetail = gShoppingBag.find(item => item.id == vIdOfPizza);
    for (let bI2 in gShoppingBag) {
        if (gShoppingBag[bI2].id == vIdOfPizza) {
            gShoppingBag[bI2].quantity = gShoppingBag[bI2].quantity + 1;
            gShoppingBag[bI2].totalOfOneType = gShoppingBag[bI2].price * gShoppingBag[bI2].quantity;
            localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
            window.location.reload();
        }
    }

}

function removeItem(paramElement) {
    var vQuantityPlace = getQuantityByParents(paramElement);
    var vIdOfPizza = $(paramElement).data('id');
    var vPizzaDetail = gShoppingBag.find(item => item.id == vIdOfPizza);
    for (let bI in gShoppingBag) {
        if (gShoppingBag[bI].id == vIdOfPizza) {
            gShoppingBag.splice(bI, 1);
            localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
            window.location.reload();
        }
    }

}

function reduceTheQuantity(paramElement) {
    var vQuantityPlace = getQuantityByParents(paramElement);
    var vIdOfPizza = $(paramElement).data('id');
    var vPizzaDetail = gShoppingBag.find(item => item.id == vIdOfPizza);
    if (vPizzaDetail.quantity == 1) {
        for (let bI in gShoppingBag) {
            if (gShoppingBag[bI].id == vIdOfPizza) {
                gShoppingBag.splice(bI, 1);
                localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
                window.location.reload();
            }
        }
    }
    if (vPizzaDetail.quantity >= 2) {
        for (let bI2 in gShoppingBag) {
            if (gShoppingBag[bI2].id == vIdOfPizza) {
                gShoppingBag[bI2].quantity = gShoppingBag[bI2].quantity - 1;
                gShoppingBag[bI2].totalOfOneType = gShoppingBag[bI2].price * gShoppingBag[bI2].quantity;
                localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
                window.location.reload();
            }
        }

    }
}

function getQuantityByParents(element) {
    var vParent = $(element).parents('.line--anProduct__quantityControl');
    var vShowQuantity = $(vParent).children('.anProduct__quantityControl--btn-show');
    return vShowQuantity;
}

function callAPIAndloadPizzaProductsIntoFE() {
    var vAPI_URL = gBASE_URL + "/pizza";
    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        async: false,
        success: function (res) {
            console.log(res);
            loadPizzaProducts(res);
            gPizzaOrderList = res;
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function loadPizzaProducts(pizzaList) {
    $(".list-pizza-kitchen-section").html("");
    var vProduct = "";
    for (var i in pizzaList) {
        vProduct += `
        <div class="product-detail-kitchen">
        <div class="img-of-pizza-detail">
            <img alt="" src="${pizzaList[i].imageUrl}">
        </div>
        <div class="information-each-pizza-detail">
            <div>
                <span class="title-pizza-kitchen">${pizzaList[i].name}</span>
                <span class="title-pizza-kitchen">$${pizzaList[i].price}</span>
            </div>
            <div>
                <div>
                    <span class="has-border-detail"><i class="fa-solid fa-star"></i>&nbsp;${pizzaList[i].rating}</span>
                    <span class="has-border-detail">${pizzaList[i].time}</span>
                </div>
                <button data-id=${pizzaList[i].id} class="add-a-product">
                    <span class="choosing-add-outside"><i class="fa-solid fa-plus"></i></span>
                </button>
            </div>
        </div>
    </div>

        `
    }
    $(".list-pizza-kitchen-section").html(vProduct);

}

function callAPIAndloadBlogIntoFE() {
    var vAPI_URL = gBASE_URL + "/blogs";
    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        success: function (res) {
            console.log(res);
            loadBlog(res);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function loadBlog(listBlog) {
    $(".blog-card-left-list").html("");
    var vBlogList = "";
    for (let i in listBlog) {
        if (i < 2) {
            vBlogList += `
            <div class="blog-card-left-1">
                <div >
                    <picture class="blog-card-picture"><img src="${listBlog[i].imageUrl}"></picture>
                </div>
                <div class="blog-card-information">
                    <h5>${listBlog[i].title} </h5>
                    <span class="blog-p">
                        <p>${listBlog[i].description}</p>
                    </span>
                </div>
            </div>
`
        }
    }
    $(".blog-card-left-list").html(vBlogList);

    $(".blog-card-center").html("");
    var vBlogList = "";
    for (let i in listBlog) {
        if (i == 2) {
            vBlogList += `
            <picture class="middle-pic-blog-section"><img alt="" src="${listBlog[i].imageUrl}"></picture>
            <div class="blog-card-information">
                <h5>${listBlog[i].title}</h5>
                <span class="blog-p">
                    <p>${listBlog[i].description}</p>
                </span>
            </div>
`
        }
    }
    $(".blog-card-center").html(vBlogList);

    $(".blog-card-right-list").html("");
    var vBlogList = "";
    for (let i in listBlog) {
        if (i > 2) {
            vBlogList += `
            <div class="blog-card-right-1">
            <div >
                <picture class="blog-card-picture"><img src="${listBlog[i].imageUrl}"></picture>
            </div>
            <div class="blog-card-information">
                <h5>${listBlog[i].title} </h5>
                <span class="blog-p">
                    <p>${listBlog[i].description}</p>
                </span>
            </div>
        </div>
`
        }
    }
    $(".blog-card-right-list").html(vBlogList);


}

function addAProductIntoShoppingBag(element) {
    var vPizzaId = $(element).data("id");
    console.log(vPizzaId);
    const chosenItem = gShoppingBag.find(item => item.id == vPizzaId);
    if (chosenItem == undefined) {
        // them vao gShoppingBag
        for (let i in gPizzaOrderList) {
            if (vPizzaId == gPizzaOrderList[i].id) {
                gPizzaOrderList[i].quantity = 1;
                gPizzaOrderList[i].totalOfOneType = gPizzaOrderList[i].price * gPizzaOrderList[i].quantity;
                gShoppingBag.push(gPizzaOrderList[i]);
                localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
            }
        }
        // window.location.href = window.location.href;
        return chosenItem;
    }
    else {
        for (let bI2 in gShoppingBag) {
            if (gShoppingBag[bI2].id == vPizzaId) {
                gShoppingBag[bI2].quantity = gShoppingBag[bI2].quantity + 1;
                gShoppingBag[bI2].totalOfOneType = gShoppingBag[bI2].price * gShoppingBag[bI2].quantity;
                localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
            }
        }
    }
}

function changeBagToYellow() {
    if (gShoppingBag.length > 0) {
        $("#shopping-bag-icon").attr('src', `images/yellow-shopping-bag.png`)
    }
    else {
        $("#shopping-bag-icon").attr('src', `images/akar-icons_shopping-bag-white.png`);
    }
}

function loadOrderListIntoOrderPlace() {
    $(".orderPlace__header--line").html('');
    var orderLocalStorage = '';
    for (let i in gShoppingBag) {
        orderLocalStorage += `                    <div class="line--anProduct">
        <div class="line--anProduct__mainDetail col-6">
            <img alt="" src="${gShoppingBag[i].imageUrl}">
            <div>
                <span class="title-1">${gShoppingBag[i].name}</span>
                <p>beef patties, Iceberg lettuce, American cheese, pickles, ...</p>
            </div>
        </div>
        <div class="col-6 line--anProduct__otherDetail">
            <div class="col-2 text-dollar total-cost-for-a-pizza">$${gShoppingBag[i].totalOfOneType}</div>
            <div class="col-2">
                <div class="line--anProduct__quantityControl">
                    <button class="anProduct__quantityControl--btn-down" data-id='${gShoppingBag[i].id}'>-</button>
                    <span class="anProduct__quantityControl--btn-show">${gShoppingBag[i].quantity}</span>
                    <button class="anProduct__quantityControl--btn-up" data-id='${gShoppingBag[i].id}'>+</button>
                </div>
                <button class="an-Product__removeItem" data-id='${gShoppingBag[i].id}'>Remove Item</button>
            </div>
            <div class="col-2 text-dollar">$${gShoppingBag[i].price}</div>
        </div>
    </div>

        `
    };
    $(".orderPlace__header--line").html(orderLocalStorage);

}

function calculateTheLastTotalCost() {
    var vSubTotal = JSON.parse($("#sub-total-first").text());
    var vVoucherIs = $("#voucher-result").data("is-selected");
    var vDivision = gVoucherDiscountPercent / 100;
    if (vVoucherIs == "N") {
        var vTotal = vSubTotal + 20
        $("#sub-total-with-coupon").html(vTotal);
    }
    if (vVoucherIs == "Y") {
        var vTotal = vSubTotal + 20 + vDivision;
        $("#sub-total-with-coupon").html(vTotal);
    }
}